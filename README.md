                                   COMO EXECUTAR O TERRAFORM


1. EXPORTE SUAS CREDENCIAIS E  REGIAO COM:

$ export AWS_ACCESS_KEY_ID="Accesskey"


$ export AWS_SECRET_ACCESS_KEY="Secretkey"


$ export AWS_DEFAULT_REGION="us-east-1"

2. COM O COMANDO $ terrafom init VOCE VAI INICIALIZAR O DIRETÓRIO DE TRABALHO CONTENDO OS ARQUIVOS DE CONFIGURAÇÃO DO TERRAFORM
 
3. EM SEGUIDA $ terraform plan DEPOIS $ terraform apply

4. DÊ PERMISSAO A SSH KEYS QUE SERA GERADA APÓS O APPLY $ chmod 400 chave.pem

5. ESPERE 1 MINUTO E EXECUTE O ANSIBLE $ ansible-playbook -i hosts playbook.yml 
 

---

                                   ACESSO AO BANCO APÓS A CRIAÇÃO

ASSIM QUE FOR CRIADO A LOJA, ACESSE O IP DA INSTÂNCIA.

NAS CONFIGURAÇÕES O ENDPOINT JÁ ESTARÁ APONTANDO PARA O RDS

CREDENCIAIS DO BANCO POR PADRÃO DEIXEI COMO:
 
DATABASE= magentodb
USER NAME= magento
USER PASSWORD= magento123


## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).
