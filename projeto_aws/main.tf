provider "aws"{} 

resource "aws_vpc" "vpc_magento" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  instance_tenancy = "default" 
 
  tags {
    Name = "vpc_magento"
  }

}

resource "aws_subnet" "subnet_pub" {
  vpc_id     = "${aws_vpc.vpc_magento.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "${var.az_subnet}"
  map_public_ip_on_launch = true
  depends_on = ["aws_vpc.vpc_magento"]

  tags {
    Name = "Subnet_pub"
  }
}

resource "aws_subnet" "subnet_priv" {
  vpc_id     = "${aws_vpc.vpc_magento.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "${var.az_subnet2}"
  
  tags {
    Name = "Subnet_priv"
  }
  depends_on = ["aws_vpc.vpc_magento"]
}

resource "aws_subnet" "subnet_priv2" {
  vpc_id     = "${aws_vpc.vpc_magento.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "${var.az_subnet3}"

  tags {
    Name = "Subnet_priv2"
  }
  depends_on = ["aws_vpc.vpc_magento"]
}


resource "aws_internet_gateway" "internet_gateway_magento" {
  vpc_id = "${aws_vpc.vpc_magento.id}"
  depends_on = ["aws_vpc.vpc_magento"]
  tags {
    Name = "igw_magento"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = "${aws_vpc.vpc_magento.id}"
  depends_on = ["aws_vpc.vpc_magento"]
  depends_on = ["aws_internet_gateway.internet_gateway_magento"]
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.internet_gateway_magento.id}"
  }

  tags {
    Name = "route_table"
  }
}

resource "aws_route_table_association" "associacao_rotas" 
{
 subnet_id      = "${aws_subnet.subnet_pub.id}"
 route_table_id = "${aws_route_table.route_table.id}"
 depends_on = ["aws_vpc.vpc_magento"]
 depends_on = ["aws_subnet.subnet_pub"]
 depends_on = ["aws_route_table.route_table"]
  depends_on = ["aws_internet_gateway.internet_gateway_magento"]

}

resource "aws_security_group" "sg_ec2" {
 name        = "sg_ec2"
 description = "liberacao_portas_ec2"
 vpc_id = "${aws_vpc.vpc_magento.id}"
 depends_on = ["aws_vpc.vpc_magento"]

 ingress {
   from_port   = "${var.server_port}"
   to_port     = "${var.server_port}"
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }

 ingress {
   from_port   = "${var.acess_port}"
   to_port     = "${var.acess_port}"
   protocol    = "tcp"
   cidr_blocks = "${var.cidr_sg_ec2}"
 }

 egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
  }

 tags {
   Name = "sg_ec2"
 }

}


resource "tls_private_key" "default" {
  algorithm = "RSA"
}


resource "aws_key_pair" "generated" {
  depends_on = ["tls_private_key.default"]
  key_name   = "chave"
  public_key = "${tls_private_key.default.public_key_openssh}"
}


resource "aws_instance" "magento" {
  ami = "ami-04681a1dbd79675a5"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.subnet_pub.id}"
  security_groups = ["${aws_security_group.sg_ec2.id}"] 
  depends_on = ["aws_vpc.vpc_magento"]  
  
  tags {
    Name = "Magento",
    Owner = "seu.nome"
  }
  key_name = "chave"
}


resource "local_file" "private_key_pem" {
  depends_on = ["tls_private_key.default"]
  content    = "${tls_private_key.default.private_key_pem}"
  filename   = "chave.pem"
}

resource "local_file" "ec2_ip_magento" {
  depends_on = ["aws_instance.magento"]
  content    = "${aws_instance.magento.public_ip}"
  filename   = "ec2_ip_magento"
}


resource "aws_security_group" "sg_rds" {
 name        = "sg_rds"
 description = "liberacao_portas_rds"
 vpc_id = "${aws_vpc.vpc_magento.id}"
 depends_on = ["aws_vpc.vpc_magento"]
 ingress {
   from_port   = "${var.rds_port}"
   to_port     = "${var.rds_port}"
   protocol    = "tcp"
   cidr_blocks = ["${aws_instance.magento.private_ip}/32"]
 }

 egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
  }


 tags {
   Name = "sg_rds"
 }
}

resource "aws_db_subnet_group" "subnet_db_magento" {
 name       = "subnet_db_magento"
 subnet_ids = ["${aws_subnet.subnet_priv.id}","${aws_subnet.subnet_priv2.id}"]
 depends_on = ["aws_vpc.vpc_magento"]
 tags {
   Name = "subnet magento"
 }
}
 
resource "aws_db_instance" "magentodb" {
 allocated_storage    = 10
 storage_type         = "gp2"
 engine               = "mysql"
 engine_version       = "5.6"
 instance_class       = "db.t2.micro"
 name                 = "${var.db_name}"
 username             = "${var.db_username}"
 password             = "${var.db_passwd}"
 parameter_group_name = "default.mysql5.6"
 vpc_security_group_ids = ["${aws_security_group.sg_rds.id}"]
 db_subnet_group_name = "${aws_db_subnet_group.subnet_db_magento.id}"
 depends_on = ["aws_vpc.vpc_magento"]
 depends_on = ["aws_db_subnet_group.subnet_db_magento"]
 tags {
   Name = "MagentoDB"
}

} 

resource "local_file" "rds_ip_magento" {
  depends_on = ["aws_db_instance.magentodb"]
  content    = "${aws_db_instance.magentodb.address}"
  filename   = "rds_ip_magento"
}




