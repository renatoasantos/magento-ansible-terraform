variable "server_port" {
    description = "Port_from_site"
    default = 80
}

variable "acess_port" {
     description = "Port_from_ssh"
     default = 22
} 

variable "rds_port" {
     description = "Port_from_rds"
     default = 3306
}


variable "cidr_sg_ec2" {
     description = "Cidr_block_SG_ec2_instance"
     default = ["179.191.118.22/32"]
}

variable "az_subnet" {
     description = "subnet_availability_zone"
     default = "us-east-1a"
}

variable "az_subnet2" {
     description = "subnet_availability_zone"
     default = "us-east-1b"
}

variable "az_subnet3" {
     description = "subnet_availability_zone"
     default = "us-east-1c"
}

variable "db_name" {
     description = "database_name"
     default = "magentodb"
}

variable "db_username" {
     description = "database_usarname"
     default = "magento"
}

variable "db_passwd" {
     description = "database_password"
     default = "magento123"
}


